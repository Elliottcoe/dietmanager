@extends('admin.layout.master')

@section('body')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Add Meal</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">

                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-6">
                            {{ Form::open(array('url' => 'admin/meals/add')) }}
                            <div class="form-group">
                                <label>Name</label>
                                <input name="name" class="form-control">

                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Description</label>
                                <input name="desc" class="form-control">

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label>Fat (g)</label>
                                <input name="fat" class="form-control">

                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label>Saturates (g)</label>
                                <input name="sats" class="form-control">

                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label>carbohydrates (g)</label>
                                <input name="carbs" class="form-control">

                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label>Salt (g)</label>
                                <input name="salt" class="form-control">

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="ingredients">Ingredients</label>
                                <textarea class="form-control" rows="5" id="ingredients" name="ingredients"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label for="method">Method</label>
                                <textarea class="form-control" rows="5" id="method" name="recipe"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label for="sel1">Meal:</label>
                                <select class="form-control" id="sel1" name="meal">
                                    <option>Breakfast</option>
                                    <option>Lunch</option>
                                    <option>Dinner</option>
                                    <option>Snack</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <label>Calories</label>
                                <input name="calories" class="form-control">

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <div class="radio">
                                    <label><input type="radio" name="vege" onclick='alert("Please be sure this is a vegetarian meal")'>Vegetarian</label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <input type="submit" class="btn btn-primary" value="Add">
                                <br>
                                <br>
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                            </div>
                            </div>
                        </div>
                        {{ Form::close() }}
                    </div>
                    <!-- /.row (nested) -->
                </div>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->

    </div>
    <!-- /.col-lg-12 -->

    </div>
    <!-- /.row -->

@endsection