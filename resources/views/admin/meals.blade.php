@extends('admin.layout.master')

@section('body')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Meals</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Meals
                </div>
                <div class="panel-body">
                    <div class="row">

                        <div class="col-lg-12">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Vegetarian</th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($meals as $meal)
                                <tr>
                                    <td>{{$meal->id}}</td>
                                    <td>{{$meal->name}}</td>
                                    <td>{{$meal->vegetarian}}</td>
                                    <td><a href="{{$meal->id}}">Edit</a> | <a href="{{$meal->id}}">Delete</a> | <a href={{url('admin/meals/view')}}/{{$meal->id}}>View</a></td>

                                </tr>
                                @endforeach
                                </tbody>
                            </table>


                        </div>

                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

@endsection