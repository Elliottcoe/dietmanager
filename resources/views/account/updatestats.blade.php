@extends('layout.master')

@section('body')

    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Your Stats</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <div class="row">
        <div class="col-lg-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Tell us about yourself
                </div>
                <div class="panel-body">
                    <div class="row">

                        <div class="col-lg-6">
                            {!! Form::open(array('url' => 'account/stats')) !!}
                            <div class="form-group">
                                <label>Gender</label>
                                <select name="gender" class="form-control">
                                    <option value="Female">Female</option>
                                    <option value="Male">Male</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label>Date of Birth</label>
                                <input name="dob" class="form-control" placeholder="06/01/1991">

                            </div>

                            <div class="form-group">
                                <label>Ideal Weight?</label>
                                <input name="target_weight" class="form-control" placeholder="175">

                            </div>


                        </div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label>Weight(pounds)</label>
                                <input name="weight" class="form-control">

                            </div>
                            <div class="form-group">
                                <label>Height(cm)</label>
                                <input name="height" class="form-control" placeholder="175">

                            </div>



                        </div>
                        <div class="col-lg-12">
                            <div class="form-group">
                                <input type="submit" class="btn btn-success" value="Go!">
                                <br>
                                <br>
                                @if (count($errors) > 0)
                                    <div class="alert alert-danger">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif

                            </div>
                            {!! Form::close() !!}
                        </div>

                        <!-- /.col-lg-6 (nested) -->
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->

@endsection