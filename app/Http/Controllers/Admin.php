<?php

namespace App\Http\Controllers;

use App\meal;
use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class Admin extends Controller
{
    public function Index(){
        $user = Auth::user();

            if ($user->admin == 1) {
                return view('admin.main');
            } else {
                return redirect('/');
            }

    }
    public function Meals(){
        $user = Auth::user();
        if ($user->admin == 1) {
            $meals = meal::paginate(15);
            return view('admin.meals')->with('meals',$meals);
        } else {
            return redirect('/');
        }
    }
    public function Meals_Add_Get(){
        $user = Auth::user();
        if ($user->admin == 1) {
            return view('admin.mealsadd');
        } else {
            return redirect('/');
        }
    }
    public function Meals_Add_Post(request $request){
        $user = Auth::user();
        if ($user->admin == 1) {
            $this->validate($request, [
                'name' => 'required',
                'desc' => 'required|max:150',
                'fat' => 'required',
                'sats' => 'required',
                'carbs' => 'required',
                'salt' => 'required',
                'calories' => 'required|numerical',
                'ingredients' => 'required|max:700|min:100',
                'recipe' => 'required|max:3000|min:100',
            ]);

            $meal =  new meal;

            $meal->name = $request->name;
            $meal->description = $request->desc;
            $meal->meal = $request->meal;
            $meal->fat = $request->fat;
            $meal->carbs = $request->carbs;
            $meal->saturates = $request->sats;
            $meal->salt = $request->salt;
            $meal->ingredients = $request->ingredients;
            $meal->method = $request->recipe;
            $meal->calories = $request->calories;
            $meal->save();
            return action('admin@Meals_Add_Get');

        } else {
            return redirect('/');
        }
    }
    public function Meal_View(Request $request, $id){
        $meal = meal::find($id);
        return view('admin.mealsview')->with('meal',$meal);
    }
}
