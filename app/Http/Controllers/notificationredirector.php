<?php

namespace App\Http\Controllers;

use App\user_notifications;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class notificationredirector extends Controller
{
    public function process(Request $request, $id){

        $notification = user_notifications::find($id);

        $notification->is_new = 0;

        $notification->save();
        return redirect("$notification->href");


    }

}
