<?php

namespace App\Http\Controllers;

use App\userstat;
use Illuminate\Http\Request;
use Auth;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class account extends Controller
{
    public function getStatsUpdate(){
        if(Auth::user()){
            return view('account.updatestats');
        }else{
            return redirect('login');
        }

    }
    public function postStatsUpdate(request $request){
//test

        $this->validate($request, [
            'gender' => 'required',
            'weight' => 'required|numeric',
            'target_weight' => 'required|max:3|min:3',
            'height' => 'required|max:3|min:3',
            'dob' => 'required|max:10|date'
        ]);
        $userid = Auth::user()->id;
        $weight =  $request->weight;
        $dob = $request->dob;
        $height = $request->height;
        $gender = $request->gender;
        $target = $request->target_weight;

        $stat = new userstat;

        $stat->sex = $gender;
        $stat->height = $height;
        $stat->birthdate = $dob;
        $stat->current_weight = $weight;
        $stat->target_weight = $target;
        $stat->user_id = $userid;
        $stat->save();

        return redirect('/');




    }
}
