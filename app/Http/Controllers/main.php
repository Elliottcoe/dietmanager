<?php

namespace App\Http\Controllers;

use App\userstat;
use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use App\Http\Controllers\Controller;

class main extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

    }

    public function index()
    {
        if (Auth::user()->active == 0) {
            return "User is inactive.";
        }
        $userstats = userstat::where('user_id',Auth::user()->id)->get();

        if(count($userstats)<1){
            return redirect('account/stats');
        }
        return view('main');

    }
}
