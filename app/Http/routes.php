<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/','main@index');


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    Route::get('auth/login','Auth\AuthController@getlogin');
    Route::post('auth/login','Auth\AuthController@postlogin');
    Route::get('auth/logout','Auth\AuthController@logout');
    Route::get('auth/signup','Auth\AuthController@getRegister');
    Route::post('auth/signup','Auth\AuthController@postRegister');
});

Route::group(['middleware' => 'web']    , function () {
    Route::auth();
    Route::get('/', 'main@index');

    Route::get('/admin','admin@index');
    Route::get('/admin/meals','admin@Meals');
    Route::get('/admin/meals/view/{id}','admin@Meal_View');
    Route::get('/admin/meals/add','admin@Meals_Add_Get');
    Route::post('/admin/meals/add','admin@Meals_Add_Post');
    Route::get('account/stats','Account@getStatsUpdate');
    Route::post('account/stats','Account@postStatsUpdate');
    Route::get('notification/{id}','notificationredirector@process');
});

Route::group(['middleware' => ['AdminMiddleware']] ,function () {

});
