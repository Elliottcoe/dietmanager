<?php

namespace App\Providers;
use App\userstat;
use Auth;
use App\user_notifications;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('layout.nav', function($view){
            if(Auth::user()) {
                $user = Auth::user()->id;
                $data = user_notifications::where('is_new', 1)->where('user_id', $user)->get();
                $view->with('notifications', $data);
            }
        });

        view()->composer('admin.layout.nav', function($view){
            if(Auth::user()) {
                $user = Auth::user()->id;
                $data = user_notifications::where('is_new', 1)->where('user_id', $user)->get();
                $view->with('notifications', $data);
            }
        });
        view()->composer('*', function($view) {

            if(Auth::user()) {
                $stats = userstat::where('user_id', Auth::user()->id)->first();


                if (count($stats) < 1) {
                    $view->with('admin', Auth::user()->admin);
                } else {
                    $kg = round($stats->current_weight / 2.20);
                    $m = round($stats->height / 100);
                    $bmi = round(($kg / $m) / $m);
                    if ($stats->sex == 'Male') {
                        $calories = 2300;
                    } else {
                        $calories = 1800;
                    }
                    $view->with('current_weight', $stats->current_weight)->with('bmi', $bmi)->with('admin', Auth::user()->admin)->
                    with('calories', $calories)->with('target_weight', $stats->target_weight);
                }
            }else{
                return redirect('login');
            }
        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
