<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Meals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meals', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('description');
            $table->string('meal');
            $table->string('fat');
            $table->string('carbs');
            $table->string('saturates');
            $table->string('salt');
            $table->string('calories');
            $table->LongText('ingredients');
            $table->boolean('vegetarian');
            $table->LongText('method');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
